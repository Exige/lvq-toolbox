LVQ (Learning Vector Quantization) is a form of prototype classification. LVQ tries to fit a number of prototypes to do the data that best seperate each of the classes. New (unknown) data points are assigned the class of the closest prototype. 

GMLVQ is a variant of LVQ that uses a more advanced distance measure that uses a matrix that takes into account feature relevance and correlation. One of the characteristics of this matrix is that after the training phase is completed it is dominated by very few nonzero eigenvalues, and the matrix tends to become singular in practice. This reduces overfitting effects even though GMLVQ involves a lot of parameters.


Related links for further reading:

https://en.wikipedia.org/wiki/Learning_vector_quantization

http://www.mitpressjournals.org/doi/abs/10.1162/neco.2009.10-08-892

