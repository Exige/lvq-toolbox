/*
 *  Scatter plot stuff.
 *     Copyright (C) 2017  Thomas Nijman
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package Visualization;

import java.awt.*;

import java.awt.geom.Rectangle2D;


import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.CrosshairState;
import org.jfree.chart.plot.FastScatterPlot;
import org.jfree.chart.plot.PlotRenderingInfo;
import org.jfree.ui.RectangleEdge;

public class ExtendedFastScatterPlot extends FastScatterPlot {

    /**
     *
     */
    private static final long    serialVersionUID    = 1L;


    Paint[] mColors;
    float [][] mData;
    int [] mShapes;






    public ExtendedFastScatterPlot(float [][] data,  NumberAxis domainAxis, NumberAxis rangeAxis) {
        super(data, domainAxis,rangeAxis);

        setData(data);
    }

    public void setData(float [][] data, Paint[] colors, int [] shapes)
    {
      //  mData = data;
        mColors = colors;
        mShapes = shapes;
        setData(data);
        datasetChanged(null);
    }


    @Override
    public void render(Graphics2D g2, Rectangle2D dataArea, PlotRenderingInfo info, CrosshairState crosshairState) {


        if (this.getData() != null) {
            // First paint regular points.
            for (int i = 0; i < getData()[0].length; i++) {

                float x = this.getData()[0][i];
                float y = this.getData()[1][i];

                int transX = (int) this.getDomainAxis().valueToJava2D(x, dataArea, RectangleEdge.BOTTOM);
                int transY = (int) this.getRangeAxis().valueToJava2D(y, dataArea, RectangleEdge.LEFT);


                g2.setPaint(mColors[i]);
                if(mShapes[i] == 0 )
                {
                    g2.fillRect(transX, transY, 10, 10);
                }
                else
                {
                    g2.fillOval(transX, transY, 50, 50);
                }
            }
        }
    }
}