/*
 *  LVQ system with prototypes. also defines the distance measure.
 *     Copyright (C) 2017  Thomas Nijman
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package algorithm;

import org.la4j.Vector;

import java.util.ArrayList;


public class LVQSystem
{
    ArrayList<LVQPrototype> mProtos;
    IDistanceMeasure mIDistanceMeasure;

    public IDistanceMeasure getIDistanceMeasure()
    {
        return mIDistanceMeasure;
    }


    public ArrayList<LVQPrototype> getProtos()
    {
        return mProtos;
    }

    public void setProtos(ArrayList<LVQPrototype> protos)
    {
        mProtos = protos;
    }


    public void setDistanceMeasure(IDistanceMeasure IDistanceMeasure)
    {
        mIDistanceMeasure = IDistanceMeasure;
    }

    public int evaluate(Vector vec)
    {
        double curDistance = mIDistanceMeasure.distance(vec, mProtos.get(0).getCoord());
        LVQPrototype closest = mProtos.get(0);
        for(int i = 1;i < mProtos.size();i++)
        {
            double distance = mIDistanceMeasure.distance(vec, mProtos.get(i).getCoord());
            if(distance < curDistance)
            {
                curDistance = distance;
                closest = mProtos.get(i);
            }
        }

        return closest.getLabel();
    }
}
