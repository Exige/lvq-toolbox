/*
 *  General LVQ prototype.
 *     Copyright (C) 2017  Thomas Nijman
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package algorithm;

import org.la4j.Vector;
import org.la4j.vector.dense.BasicVector;


public class LVQPrototype
{


    public Vector getCoord()
    {
        return mCoord;
    }

    public void setCoord(Vector coord)
    {
        mCoord = coord;
    }

    Vector mCoord;

    public int getLabel()
    {
        return mLabel;
    }

    int mLabel;

    public LVQPrototype(Vector vector, int label)
    {
        mCoord = vector;
        mLabel = label;
    }

    public LVQPrototype(double []coord, int label)
    {
        mCoord = Vector.fromArray(coord);
        mLabel = label;
    }
}
