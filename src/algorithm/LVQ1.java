/*
 *  LVQ1 training algorithm.
 *     Copyright (C) 2017  Thomas Nijman
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package algorithm;

import org.la4j.Vector;
import org.la4j.vector.dense.BasicVector;

import java.util.ArrayList;


public class LVQ1 extends LVQTraining
{
    public LVQ1(int numEpochs, double protoTypeLearningRate, boolean useLearningRateDecay, int[] protosPerClass, int prototypeInitialization, boolean doZTransform)
    {
        super(numEpochs, protoTypeLearningRate, useLearningRateDecay, protosPerClass, prototypeInitialization, doZTransform);
    }

    @Override
    public void train()
    {
        super.train();

        // Initialize prototypes.
        initializePrototypes(mData, mLabels);

        EuclideanDistanceMeasure distanceMeasure = new EuclideanDistanceMeasure();
        mLVQSystem.setDistanceMeasure(distanceMeasure);

        for(int epoch = 0;epoch < mNumEpochs;epoch++)
        {
            ArrayList<Integer> indices = createShuffledDataIndex();
            for(int j = 0;j < mNumSamples;j++)
            {
                int r = indices.get(j);

                Vector fVec = new BasicVector(mNumFeatures);

                for(int l = 0;l < mNumFeatures;l++)
                {
                    fVec.set(l, mData[l][r]);
                }

                int label = mLabels[r];

                LVQPrototype winner = mLVQSystem.getProtos().get(0);
                double curDistance = Double.MAX_VALUE;

                for(int i = 0; i < mLVQSystem.getProtos().size();i++)
                {
                    double distance = distanceMeasure.distance(fVec, mLVQSystem.getProtos().get(i).getCoord());

                    if(distance < curDistance)
                    {
                        // This prototype is closer.
                        curDistance = distance;
                        winner = mLVQSystem.getProtos().get(i);
                    }
                }


                // Push or repel the prototype based on the label.
                int sign = winner.getLabel() == label ? 1 : -1;

                winner.setCoord(winner.getCoord().add(fVec.subtract(winner.getCoord()).multiply(mPrototypeLearningRate).multiply(sign)));
            }
        }
    }
}
