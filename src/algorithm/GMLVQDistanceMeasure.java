/*
 *  GMLVQ adaptive distance measure.
 *     Copyright (C) 2017  Thomas Nijman
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package algorithm;

import algorithm.IDistanceMeasure;
import org.la4j.Matrix;
import org.la4j.Vector;
import org.la4j.vector.dense.BasicVector;


public class GMLVQDistanceMeasure implements IDistanceMeasure
{
    Matrix mOmega;

    public GMLVQDistanceMeasure(int numFeatures)
    {
        mOmega = Matrix.identity(numFeatures);
    }

    public Matrix getOmega()
    {
        return mOmega;
    }

    public void setOmega(Matrix omega)
    {
        mOmega = omega;
    }




    @Override
    public double distance(Vector vec1, Vector vec2)
    {
        BasicVector deltaVec = (BasicVector)vec1.subtract(vec2);

        Matrix lambda = mOmega.transpose().multiply(mOmega);
        Vector tmp = deltaVec.multiply(lambda);

        double r = tmp.innerProduct(deltaVec);

        return r;
    }
}
