/*
 *  Abstract class used to define any LVQ related learning scheme.
 *     Copyright (C) 2017  Thomas Nijman
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package algorithm;

import org.la4j.Vector;
import org.la4j.vector.dense.BasicVector;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;


public abstract class LVQTraining
{
    protected double mPrototypeLearningRate;
    protected double mLearningRateAnnealingFactor;
    int mNumEpochs;
    boolean mUseLearningRateDecay;
    int [] mProtosPerClass;
    LVQSystem mLVQSystem;
    int mNumPrototypes;
    int mPrototypeInitialization;
    int mNumSamples;
    int mNumFeatures;
    boolean mDoZTransform;

    // Training data.
    double[][] mData;
    int[] mLabels;


    // Prototype initialization types.
    public static final int PROTO_INIT_RANDOM = 0;
    public static final int PROTO_INIT_CLASS_MEANS = 1;
    public static final int PROTO_INIT_NAIVE = 2;

    public void setData(double[][] data, int[] labels)
    {
        mData = data;
        mLabels = labels;

        mNumSamples = mLabels.length;
        mNumFeatures = mData.length;
    }

    public LVQSystem getLVQSystem()
    {
        return mLVQSystem;
    }


    public void train()
    {
        if(mDoZTransform)
        {
            zScoreTransform();
        }
    }

    public LVQTraining(int numEpochs, double protoTypeLearningRate, boolean useLearningRateDecay, int []protosPerClass, int prototypeInitialization, boolean doZTransform)
    {
        mNumEpochs = numEpochs;
        mPrototypeLearningRate = protoTypeLearningRate;
        mUseLearningRateDecay = useLearningRateDecay;
        mProtosPerClass = protosPerClass;
        mLVQSystem = new LVQSystem();
        mNumPrototypes = 0;
        mDoZTransform = doZTransform;
        mPrototypeInitialization = prototypeInitialization;
        for(int i = 0;i < protosPerClass.length;i++)
        {
            mNumPrototypes++;
        }
    }

    public void setLearningRateAnnealingFactor(double learningRateAnnealingFactor)
    {
        mLearningRateAnnealingFactor = learningRateAnnealingFactor;
    }

    public void initializePrototypes(double [][] data, int[] labels)
    {
        ArrayList<LVQPrototype> lvqPrototypes = new ArrayList<>(mNumPrototypes);

        // Initialize prototypes close to class means.
        if(mPrototypeInitialization == PROTO_INIT_CLASS_MEANS)
        {
            ArrayList<Vector> means = new ArrayList<>(mNumPrototypes);
            for(int i = 0;i < mNumPrototypes;i++)
            {
                means.add(new BasicVector(data.length));
            }
            int [] numPoints = new int[mNumPrototypes];

            for(int i = 0;i < data[0].length;i++)
            {
                for(int j = 0;j < mNumPrototypes;j++)
                {
                    if(mProtosPerClass[j] == labels[i])
                    {
                        for(int l = 0;l < data.length;l++)
                        {
                            means.get(j).set(l, means.get(j).get(l) + data[l][i]);
                            numPoints[j]++;
                        }
                    }
                }
            }


            for(int i = 0;i < mNumPrototypes;i++)
            {
                means.set(i, means.get(i).divide(numPoints[i]));

                // Add random perturbation.
                for(int j = 0;j < data.length;j++)
                {
                    Random random = new Random();
                    double perturbation = (random.nextDouble()*2 - 1.0)/10.0;
                    means.get(i).set(j, means.get(i).get(j) + perturbation);
                }
                lvqPrototypes.add(new LVQPrototype(means.get(i), mProtosPerClass[i]));
            }
        }


        else if(mPrototypeInitialization == PROTO_INIT_NAIVE)
        {
            for (int i = 0; i < mNumPrototypes; i++)
            {
                Vector vec = new BasicVector(data.length);
                for (int j = 0; j < data.length; j++)
                {
                    vec.set(j, data[j][i]);
                }
                lvqPrototypes.add(new LVQPrototype(vec, mProtosPerClass[i]));
            }
        }

        mLVQSystem.setProtos(lvqPrototypes);
    }



    protected void annealLearningRate(int t)
    {
        mPrototypeLearningRate = mPrototypeLearningRate / (1 + mLearningRateAnnealingFactor*(t - 1));
    }


    public double computeTrainingError()
    {
        double numCorrect = 0;
        double numIncorrect = 0;
        for(int l = 0;l < mData[0].length;l++)
        {
            double []input = new double[mData.length];
            for(int i = 0;i < mData.length;i++)
            {
                input[i] = mData[i][l];
            }

            org.la4j.Vector vec = org.la4j.Vector.fromArray(input);

            int label = mLVQSystem.evaluate(vec);
            if(label == mLabels[l])
            {
                numCorrect++;
            }
            else
            {
                numIncorrect++;
            }
        }

        // Compute error.
        double prctg = numIncorrect / (numCorrect + numIncorrect);

        return prctg;
    }


    protected ArrayList<Integer> createShuffledDataIndex()
    {
        // generate list of indices.
        ArrayList<Integer> indices = new ArrayList<>();
        for(int j = 0;j < mNumSamples;j++)
        {
            indices.add(j);
        }

        Collections.shuffle(indices);
        return indices;
    }


    // generates 2D vectors taht can be used for visualisation
    public float[][] generate2DVectors()
    {
        float[][] fData = new float[2][mData[0].length + mLVQSystem.getProtos().size()];
        for(int i = 0;i < mData[0].length;i++)
        {
            fData[0][i] = (float)mData[0][i];
            fData[1][i] = (float)mData[1][i];
        }

        for(int i = 0;i < mLVQSystem.getProtos().size();i++)
        {
            fData[0][i + mData[0].length] = (float)mLVQSystem.getProtos().get(i).getCoord().get(0);
            fData[1][i + mData[1].length] = (float)mLVQSystem.getProtos().get(i).getCoord().get(1);
        }

        return fData;
    }

    // Performs a z score transformation of the feature vectors.
    private void zScoreTransform()
    {
        Vector meanVec;

        double []  mean = new double[mNumFeatures];
        double[] std = new double[mNumFeatures];

        for(int i = 0;i < mNumSamples;i++)
        {
            for(int j = 0;j < mNumFeatures;j++)
            {
                mean[j] += mData[j][i];

            }
        }

        meanVec = Vector.fromArray(mean);

        for(int i = 0;i < mNumSamples;i++)
        {
            for(int j = 0;j < mNumFeatures;j++)
            {
                std[j] += Math.pow(mData[j][i] - meanVec.get(j), 2);
            }
        }
        double a = 1.0/(double)mNumSamples;


        for(int j = 0;j < mNumFeatures;j++)
        {
            std[j] = std[j]*1.0/(double)mNumSamples;
            std[j] = Math.sqrt(std[j]);
        }

        // Transform each feature.
        for(int i = 0;i < mNumSamples;i++)
        {
            for(int j = 0;j < mNumFeatures;j++)
            {
                mData[j][i] = (mData[j][i] - meanVec.get(j))/std[j];
            }
        }

    }


}
