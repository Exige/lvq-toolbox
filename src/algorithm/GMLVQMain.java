/*
 *  GMLVQ test examples.
 *     Copyright (C) 2017  Thomas Nijman
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package algorithm;

import Visualization.ExtendedScatterPlotFrame;
import org.la4j.Matrix;
import org.la4j.decomposition.EigenDecompositor;
import org.la4j.vector.dense.BasicVector;

import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;


public class GMLVQMain
{
    static ExtendedScatterPlotFrame frame;
    public static void main(String [ ] args) throws InterruptedException
    {
        int[] protosPerClass = {0, 1};

        double[][] data = readCSV("data.csv");
        data = convertData(data);


        int []labels = readLabels("labels.csv", data[0].length);

        GMLVQTraining gmlvqTraining = new GMLVQTraining(10000, 0.005, false, protosPerClass, 0.001, LVQTraining.PROTO_INIT_NAIVE, false, false);
        gmlvqTraining.setData(data, labels);
        gmlvqTraining.setRegularizationRate(0.005);
        gmlvqTraining.setLearningRateAnnealingFactor(0.0001);
        gmlvqTraining.train();


     //   LVQ1 lvq1 = new LVQ1(100, 0.005, false, protosPerClass, LVQTraining.PROTO_INIT_CLASS_MEANS, false);
       // lvq1.setData(data, labels);
       // lvq1.train();

     //  float[][] fData = lvq1.generate2DVectors();

        float [][]proj = gmlvqTraining.generate2DVectors();
        visualize(proj, labels, gmlvqTraining.getLVQSystem());


    }


    public static double[][]readCSV(String fileName)
    {
        // -define .csv file in app
        String fileNameDefined = fileName;
        // -File class needed to turn stringName to actual file
        File file = new File(fileNameDefined);
        Vector<Float[]> data = new Vector<>();

        int i = 0;

        try{
            // -read from filePooped with Scanner class
            Scanner scanner = new Scanner(file);
            // hashNext() loops line-by-
            scanner.useDelimiter(",|\r\n");
            while(scanner.hasNext()){
                //read single line, put in string
                String nStr = scanner.next();
                if(i % 2 == 0)
                {
                    data.add(new Float[2]);
                }

                data.lastElement()[i%2] = Float.parseFloat(nStr);
                i++;
            }
            // after loop, close scanner
            scanner.close();


        }catch (FileNotFoundException e){

            e.printStackTrace();
        }

        double[][]out = new double[data.size()][2];
        for(i = 0;i < data.size();i++)
        {
            out[i][0] = data.get(i)[0];
            out[i][1] = data.get(i)[1];
        }
        return out;
    }

    public static int[] readLabels(String fileName, int numElements)
    {
        // -define .csv file in app
        String fileNameDefined = fileName;
        // -File class needed to turn stringName to actual file
        File file = new File(fileNameDefined);
        int[] labels = new int[numElements];

        int i = 0;

        try{
            // -read from filePooped with Scanner class
            Scanner scanner = new Scanner(file);
            // hashNext() loops line-by-
            scanner.useDelimiter(",|\r\n");
            while(scanner.hasNext()){
                //read single line, put in string
                String nStr = scanner.next();
                labels[i] = Integer.parseInt(nStr);
                i++;
            }
            // after loop, close scanner
            scanner.close();


        }catch (FileNotFoundException e){

            e.printStackTrace();
        }

        return labels;

    }

    // Find inter class distances.

    public static double[][] convertData(double[][]data)
    {
        double[][]out = new double[2][data.length];
        for(int i = 0; i < data.length;i++)
        {
            out[0][i] = data[i][0];
            out[1][i] = data[i][1];
        }
        return out;
    }

    public static void visualize(float[][] data, int [] labels, LVQSystem lvq)
    {
        int len = data[0].length + lvq.getProtos().size();
        Paint [] colors = new Paint[len];
        int [] shapes = new int[len];

        for(int i = 0;i < labels.length;i++)
        {
            if(labels[i] == 0)
            {
                colors[i] = Color.RED;
            }
            else
            {
                colors[i] = Color.BLUE;
            }

            shapes[i] = 0;
        }

        for(int i = 0;i < lvq.getProtos().size();i++)
        {
            if(lvq.getProtos().get(i).getLabel() == 0)
            {
                colors[i + labels.length] = Color.RED;
            }
            else
            {
                colors[i + labels.length] = Color.BLUE;
            }
            shapes[i + labels.length] = 1;
        }



        if(frame == null)
        {
            frame = new ExtendedScatterPlotFrame("lel", data);
            frame.pack();
            frame.setLocationRelativeTo(null);
            frame.setVisible(true);
        }
        frame.setData(data, colors, shapes);

    }
}
