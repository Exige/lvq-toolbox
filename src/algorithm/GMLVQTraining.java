
/*
 *     GMLVQ training scheme.
 *     Copyright (C) 2017  Thomas Nijman
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package algorithm;
import Visualization.ExtendedScatterPlotFrame;
import org.la4j.LinearAlgebra;
import org.la4j.Matrix;
import org.la4j.Vector;
import org.la4j.decomposition.EigenDecompositor;
import org.la4j.inversion.GaussJordanInverter;
import org.la4j.inversion.NoPivotGaussInverter;
import org.la4j.matrix.dense.Basic2DMatrix;
import org.la4j.vector.dense.BasicVector;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class GMLVQTraining extends LVQTraining
{
    double mMatrixLearningRate;
    boolean mDoRegularization;

    // Regularization control parameter.
    // Higher -> stronger control of first m imbalanced eigenvalues of lambda.
    double mRegularizationRate;

    public void setRegularizationRate(double regularizationRate)
    {
        mRegularizationRate = regularizationRate;
    }


    public GMLVQTraining(int numEpochs, double protoTypeLearningRate, boolean useLearningRateDecay, int[] protosPerClass, double matrixLearningRate, int prototypeInitialization, boolean doZTransform,
                         boolean useRegularization)
    {
        super(numEpochs, protoTypeLearningRate, useLearningRateDecay, protosPerClass, prototypeInitialization, doZTransform);
        mMatrixLearningRate = matrixLearningRate;
        mDoRegularization = useRegularization;
    }

    @Override
    protected void annealLearningRate(int t)
    {
        super.annealLearningRate(t);
        mMatrixLearningRate = mMatrixLearningRate / (1 + mLearningRateAnnealingFactor*(t - 1));

        if(!mDoRegularization)
        {
            mRegularizationRate = 0.0;
        }
    }

    public void init()
    {

    }


    @Override
    public void train()
    {
        super.train();

        // Initialize prototypes.
        initializePrototypes(mData, mLabels);

        // Create GMLVQ system object.
        GMLVQDistanceMeasure distanceMeasure = new GMLVQDistanceMeasure(mNumFeatures);
        mLVQSystem.setDistanceMeasure(distanceMeasure);

        Matrix omega = distanceMeasure.getOmega();
        omega = normalizeOmega(omega);
        distanceMeasure.setOmega(omega);

        for(int epoch = 0;epoch < mNumEpochs;epoch++)
        {
            System.out.println("Epoch: " + Integer.toString(epoch) + ".");
            Double err = computeTrainingError();
            System.out.println("training error: " + err.toString());


            ArrayList<Integer> indices = createShuffledDataIndex();

            for(int j = 0;j < mNumSamples;j++)
            {

                int r = indices.get(j);
                Vector fVec = new BasicVector(mNumFeatures);

                for(int l = 0;l < mNumFeatures;l++)
                {
                    fVec.set(l, mData[l][r]);
                }

                int label = mLabels[r];

                LVQPrototype closestCorrect = mLVQSystem.getProtos().get(0);
                LVQPrototype closestInCorrect = mLVQSystem.getProtos().get(0);

                // Distance to closest correct prototype.
                double dJ =  Double.MAX_VALUE;

                // Distance to closest incorrect prototype.
                double dK = Double.MAX_VALUE;
                Matrix lambda = omega.transpose().multiply(omega);
                // Find closest prototype.

                for(int i = 0; i < mLVQSystem.getProtos().size();i++)
                {
                    double distance = distanceMeasure.distance(fVec, mLVQSystem.getProtos().get(i).getCoord());

                    if(distance < dJ && mLVQSystem.getProtos().get(i).getLabel() == label)
                    {
                        // This prototype is closer.
                        dJ  = distance;
                        closestCorrect = mLVQSystem.getProtos().get(i);
                    }
                    if(distance < dK && mLVQSystem.getProtos().get(i).getLabel() != label)
                    {
                        // This prototype is closer.
                        dK  = distance;
                        closestInCorrect = mLVQSystem.getProtos().get(i);
                    }
                }


                Vector wJ = closestCorrect.getCoord();
                Vector wK = closestInCorrect.getCoord();

                double normFactor = Math.pow(dJ + dK, 2);

                Vector DJ = fVec.subtract(wJ);
                Vector DK = fVec.subtract(wK);

                double muJ = 4.0*((dK)/(normFactor));
                double muK = 4.0*((dJ)/(normFactor));

                // Update closest correct and wrong prototype.
                double cons = mPrototypeLearningRate*muJ;


                // Update correct prototype.
                wJ = wJ.add(lambda.multiply(cons).multiply(DJ));
                closestCorrect.setCoord(wJ);


                cons = mPrototypeLearningRate*muK;
                // Update incorrect prototype.

                wK = wK.subtract(lambda.multiply(cons).multiply(DK));
                closestInCorrect.setCoord(wK);

                Matrix tst = new Basic2DMatrix(2, 3);

                tst.set(0, 0, 1);
                tst.set(0, 1, 7);
                tst.set(0, 2, 3);

                tst.set(1, 0, 4);
                tst.set(1, 1, 3);
                tst.set(1, 2, 10);

                // compute pseudo inverse of omega, if this option has been set.
                Matrix omegaPInv = Matrix.zero(omega.rows(), omega.columns());
                if(mDoRegularization)
                {
                    // Probably not the most efficient way to compute the pseudo inverse, but it is simple.
                    omegaPInv = omega.transpose().multiply(omega.multiply(omega.transpose()).withInverter(LinearAlgebra.InverterFactory.GAUSS_JORDAN).inverse());
                }

                // Update the omega matrix.
                for(int l = 0;l < omega.columns();l++)
                {
                    for(int m = 0;m < omega.rows();m++)
                    {
                        double f1 = muJ*(DJ.get(m)*(omega.multiply(DJ)).get(l));
                        double f2 = muK*((DK.get(m))*(omega.multiply(DK)).get(l));



                        double regTerm = mMatrixLearningRate*mRegularizationRate*omegaPInv.get(m, l);
                        double newVal = omega.get(l, m) - mMatrixLearningRate*(f1 - f2) + regTerm;
                        omega.set(l, m, newVal);

                    }
                }

                // normalize omega.
                omega = normalizeOmega(omega);

                // Update omega.
                distanceMeasure.setOmega(omega);
            }

            if(mUseLearningRateDecay)
            {
                // End of epoch, anneal learning rates, if necessary.
                annealLearningRate(epoch);
            }
        }
    }

    // Normalizes omega such that trace(lambda) = 1.
    private Matrix normalizeOmega(Matrix omega)
    {
        Matrix lambda = omega.transpose().multiply(omega);

        // normalize omega.
        double sum = 0;
        for(int i = 0;i < lambda.rows();i++)
        {
            sum += lambda.get(i, i);
        }
        sum = Math.sqrt(sum);
        omega = omega.divide(sum);
        return omega;
    }

    @Override
    public float[][] generate2DVectors()
    {
        GMLVQDistanceMeasure distanceMeasure = (GMLVQDistanceMeasure)mLVQSystem.getIDistanceMeasure();
        Matrix lambda = ((GMLVQDistanceMeasure) mLVQSystem.getIDistanceMeasure()).getOmega().transpose().multiply(distanceMeasure.getOmega());
        // Find eigenvalues and eigenvectors.
        EigenDecompositor eigenDecompositor = new EigenDecompositor(lambda);
        Matrix eig[] = eigenDecompositor.decompose();
        ArrayList<ValueIndex> diag = new ArrayList<>();

        for(int i = 0;i < eig[1].rows();i++)
        {
            diag.add(new ValueIndex(i, eig[1].get(i, i)));
        }

        Collections.sort(diag, new Comparator<ValueIndex>()
        {
            @Override
            public int compare(ValueIndex o1, ValueIndex o2)
            {
                return Double.compare(o2.value, o1.value);
            }
        });


        org.la4j.Vector vec1 = eig[0].getColumn(diag.get(0).index);
        org.la4j.Vector vec2 = eig[0].getColumn(diag.get(1).index);


        double scale1 = Math.sqrt(diag.get(0).value);
        double scale2 = Math.sqrt(diag.get(1).value);


        // Num of examples + num of prototypes.
        int len = mData[0].length + mLVQSystem.getProtos().size();

        float[][] proj = new float[2][len];



        for(int i = 0;i < mData[0].length;i++)
        {
            org.la4j.Vector vec = new BasicVector(mData.length);
            for(int j = 0;j < mData.length;j++)
            {
                vec.set(j, mData[j][i]);
            }

            proj[0][i] = (float) (scale1*vec.innerProduct(vec1));
            proj[1][i] = (float)(scale2*vec.innerProduct(vec2));
        }
        for(int i = mData[0].length;i < len;i++)
        {
            org.la4j.Vector vec = mLVQSystem.getProtos().get(i-mData[0].length).getCoord();
            proj[0][i] = (float)(scale1*vec.innerProduct(vec1));
            proj[1][i] = (float)(scale2*vec.innerProduct(vec2));
        }


        Paint[] colors = new Paint[len];
        int [] shapes = new int[len];

        return proj;
    }
}
